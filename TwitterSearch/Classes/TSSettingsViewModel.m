//
//  TSSettingsViewModel.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "TSSettingsViewModel.h"

@interface TSSettingsViewModel ()

@property (assign, nonatomic) BOOL loading;

@property (assign, nonatomic) BOOL displayUserImagesEnabled;

@property (strong, nonatomic) id<RACSubscriber> displayImagesStateChangesSubscriber;

@end

@implementation TSSettingsViewModel

- (void)dealloc
{
    [self.displayImagesStateChangesSubscriber sendCompleted];
}

- (instancetype)init
{
    return [self initWithDisplayImagesStateChangesSubscriber:nil];
}

- (instancetype)initWithDisplayImagesStateChangesSubscriber:(id<RACSubscriber>)displayImagesStateChangesSubscriber
{
    self = [super init];
    if (self) {
        self.displayImagesStateChangesSubscriber = displayImagesStateChangesSubscriber;
    }
    return self;
}

- (RACSignal *)prepareForUse
{
    return [[[[[self.keyValueStorage objectForKey:TSKeyValueStorageKeyDisplayUserImagesEnabled]
        doNext:^(NSNumber *enabled) {
            self.displayUserImagesEnabled = [enabled boolValue];
        }]
        initially:^{
            self.loading = YES;
        }]
        finally:^{
            self.loading = NO;
        }]
        ignoreValues];
}

- (RACSignal *)changeDisplayImagesState
{
    return [[RACSignal
        defer:^RACSignal *{
            return [self.keyValueStorage setObject:@(!self.displayUserImagesEnabled) forKey:TSKeyValueStorageKeyDisplayUserImagesEnabled];
        }]
        doCompleted:^{
            self.displayUserImagesEnabled = !self.displayUserImagesEnabled;
            [self.displayImagesStateChangesSubscriber sendNext:@(self.displayUserImagesEnabled)];
        }];
}

@end
