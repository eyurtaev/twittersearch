//
//  TSTweetItem.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TSTweet;

NS_ASSUME_NONNULL_BEGIN

@interface TSTweetItem : NSObject

@property (copy, nonatomic, nullable, readonly) NSString *userName;

@property (strong, nonatomic, nullable, readonly) NSURL *userImageURL;

@property (copy, nonatomic, nullable, readonly) NSString *text;

- (instancetype)initWithUserName:(NSString *__nullable)userName userImageURL:(NSURL *__nullable)userImageURL text:(NSString *__nullable)text NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
