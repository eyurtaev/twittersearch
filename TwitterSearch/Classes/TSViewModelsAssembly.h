//
//  TSViewModelsAssembly.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <Typhoon/Typhoon.h>

@protocol RACSubscriber;
@class TSTweetsSearchViewModel;
@class TSSettingsViewModel;

@interface TSViewModelsAssembly : TyphoonAssembly

- (TSTweetsSearchViewModel *)tweetsSearchViewModel;

- (TSSettingsViewModel *)settingsViewModelWithDisplayImagesStateChangesSubscriber:(id<RACSubscriber>)displayImagesStateChangesSubscriber;

@end
