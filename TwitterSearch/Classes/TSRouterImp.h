//
//  TSRouterImp.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSRouter.h"

@class TSViewControllersAssembly;

NS_ASSUME_NONNULL_BEGIN

@interface TSRouter : NSObject<TSRouter>

@property (strong, nonatomic, nullable) TSViewControllersAssembly *viewControllersAssembly;

- (instancetype)initWithWindow:(UIWindow *)window NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
