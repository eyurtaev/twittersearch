//
//  TSViewController.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 18/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "TSViewController.h"

@interface TSViewController ()

@property (strong, nonatomic) TSViewModel *viewModel;

@property (strong, nonatomic) UIView *throbberView;

@property (assign, nonatomic) BOOL isDisplayThrobberView;

@end

@implementation TSViewController

- (instancetype)initWithViewModel:(TSViewModel *)viewModel
{
    self = [super init];
    if (self) {
        self.viewModel = viewModel;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureUI];
    [self configureBinding];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
    [activityIndicator sizeToFit];
    [activityIndicator startAnimating];
    
    self.throbberView = [[UIView alloc] initWithFrame:self.view.bounds];
    self.throbberView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.throbberView.backgroundColor = [UIColor whiteColor];
    activityIndicator.center = CGPointMake(CGRectGetWidth(self.throbberView.bounds) * 0.5, CGRectGetHeight(self.throbberView.bounds) * 0.5);
    [self.throbberView addSubview:activityIndicator];
    
    RAC(self, isDisplayThrobberView) = [self throbberViewDisplayed];
}

- (void)configureUI
{
}

- (void)configureBinding
{
}

- (RACSignal *)throbberViewDisplayed
{
    return [RACSignal return:@NO];
}

#pragma mark - Properties

- (void)setIsDisplayThrobberView:(BOOL)isDisplayThrobberView
{
    if (_isDisplayThrobberView == isDisplayThrobberView) {
        return;
    }
    _isDisplayThrobberView = isDisplayThrobberView;
    
    if (_isDisplayThrobberView) {
        self.throbberView.frame = self.view.bounds;
        [self.view addSubview:self.throbberView];
    } else {
        [self.throbberView removeFromSuperview];
    }
}

@end
