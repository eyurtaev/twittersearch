//
//  TSCacheImp.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 18/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <FMDB/FMDB.h>
#import <Mantle/Mantle.h>
#import <MTLFMDBAdapter/MTLFMDBAdapter.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

#import "TSCacheImp.h"

@interface TSCache ()

@property (strong, nonatomic) FMDatabaseQueue *database;

@end

@implementation TSCache

- (instancetype)init
{
    NSAssert(NO, @"Use `initWithPath:`");
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wnonnull"
    return [self initWithPath:nil];
#pragma clang diagnostic pop
}

- (instancetype)initWithDatabaseName:(NSString *)databaseName
{
    NSParameterAssert(databaseName);
    
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:databaseName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSURL *databaseURL = [[NSBundle bundleForClass:[self class]] URLForResource:databaseName.stringByDeletingPathExtension withExtension:databaseName.pathExtension];
        NSAssert(databaseURL, @"Database file should contained in bundle of application");
        
        NSError *error;
        BOOL success = [[NSFileManager defaultManager] copyItemAtURL:databaseURL toURL:[NSURL fileURLWithPath:path] error:&error];
        if (!success) {
            NSLog(@"%@", error);
        }
    }
    return [self initWithPath:path];
}

- (instancetype)initWithPath:(NSString *)path
{
    NSParameterAssert(path);
    
    self = [super init];
    if (self) {
        self.database = [FMDatabaseQueue databaseQueueWithPath:path];
        NSAssert(self.database, @"Could not create file with path: %@", path);
    }
    return self;
}

- (RACSignal *)saveObject:(MTLModel<MTLFMDBSerializing> *)object
{
    return [self saveObjects:@[ object ]];
}

- (RACSignal *)saveObjects:(NSArray<MTLModel<MTLFMDBSerializing> *> *)objects
{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        RACSerialDisposable *disposable = [[RACSerialDisposable alloc] init];
        
        [self.database inTransaction:^(FMDatabase *db, BOOL *rollback) {
            if (disposable.disposed) {
                return;
            }
            for (MTLModel<MTLFMDBSerializing> *object in objects) {
                NSString *deleteStament = [MTLFMDBAdapter deleteStatementForModel:object];
                NSArray *deleteParameters = [MTLFMDBAdapter primaryKeysValues:object];
                [db executeUpdate:deleteStament withArgumentsInArray:deleteParameters];
                
                NSString *insertStatment = [MTLFMDBAdapter insertStatementForModel:object];
                NSArray *insertParameters = [MTLFMDBAdapter columnValues:object];
                BOOL success = [db executeUpdate:insertStatment withArgumentsInArray:insertParameters];
                if (!success) {
                    *rollback = YES;
                    NSError *error = [db lastError];
                    [subscriber sendError:error];
                    return;
                }
            }
            [subscriber sendCompleted];
        }];
        disposable.disposable = [RACDisposable disposableWithBlock:^{
            (void)self;
        }];
        
        return disposable;
    }];
}

- (RACSignal *)allObjectsForClass:(Class)objectsClass
{
    return [[RACSignal
        defer:^RACSignal *{
            NSString *query = [NSString stringWithFormat:@"SELECT * FROM %@", [objectsClass FMDBTableName]];
            return [self executeQuery:query arguments:nil modelClass:objectsClass];
        }]
        collect];
}

#pragma mark - Private methods

- (RACSignal *)executeQuery:(NSString *)query arguments:(NSArray *)arguments modelClass:(Class)modelClass
{
    NSAssert([modelClass isSubclassOfClass:[MTLModel class]], @"`modelClass` should be is kind of `MTLModel class`. Actual: %@", [modelClass class]);
    NSAssert([modelClass conformsToProtocol:@protocol(MTLFMDBSerializing)], @"`modelClass` should conforms `MTLFMDBSerializing` protocol");
    
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        RACSerialDisposable *disposable = [[RACSerialDisposable alloc] init];
        
        [self.database inDatabase:^(FMDatabase *db) {
            if (disposable.disposed) {
                return;
            }
            
            FMResultSet *resultSet = [db executeQuery:query withArgumentsInArray:arguments];
            if (!resultSet) {
                NSError *error = [db lastError];
                [subscriber sendError:error];
                return;
            }
            
            NSError *error;
            while ([resultSet nextWithError:&error]) {
                if (error) {
                    [subscriber sendError:error];
                    return;
                }
                NSError *mappingError;
                MTLModel<MTLFMDBSerializing> *object = [MTLFMDBAdapter modelOfClass:[modelClass class] fromFMResultSet:resultSet error:&mappingError];
                if (mappingError) {
                    [subscriber sendError:error];
                    return;
                }
                if (object) {
                    [subscriber sendNext:object];
                }
                if (disposable.disposed) {
                    return;
                }
            }
            [subscriber sendCompleted];
        }];
        
        disposable.disposable = [RACDisposable disposableWithBlock:^{
            (void)self;
        }];
        
        return disposable;
    }];
}

@end
