//
//  TSCacheImp.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 18/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSCache.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSCache : NSObject<TSCache>

- (instancetype)initWithDatabaseName:(NSString *)databaseName;

- (instancetype)initWithPath:(NSString *)path NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
