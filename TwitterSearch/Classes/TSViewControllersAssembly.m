//
//  TSViewControllersAssembly.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSViewControllersAssembly.h"
#import "TSViewModelsAssembly.h"
#import "TSViewController.h"
#import "TSTweetsSearchViewController.h"
#import "TSSettingsViewController.h"

@interface TSViewControllersAssembly ()

@property (strong, nonatomic) TSViewModelsAssembly *viewModelsAssembly;

@end

@implementation TSViewControllersAssembly

- (TSViewController *)baseViewController
{
    return [TyphoonDefinition withClass:[TSViewController class] configuration:^(TyphoonDefinition *definition) {
        [definition setAbstract:YES];
    }];
}

- (TSTweetsSearchViewController *)tweetsSearchViewController
{
    return [TyphoonDefinition withClass:[TSTweetsSearchViewController class] configuration:^(TyphoonDefinition *definition) {
        definition.parent = [self baseViewController];
        
        [definition useInitializer:@selector(initWithViewModel:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[self.viewModelsAssembly tweetsSearchViewModel]];
        }];
        [definition injectProperty:@selector(title) with:NSLocalizedString(@"tweets_search.title", nil)];
    }];
}

- (TSSettingsViewController *)settingsViewControllerWithDisplayImagesStateChangesSubscriber:(id<RACSubscriber>)displayImagesStateChangesSubscriber
{
    return [TyphoonDefinition withClass:[TSSettingsViewController class] configuration:^(TyphoonDefinition *definition) {
        definition.parent = [self baseViewController];
        
        [definition useInitializer:@selector(initWithViewModel:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[self.viewModelsAssembly settingsViewModelWithDisplayImagesStateChangesSubscriber:displayImagesStateChangesSubscriber]];
        }];
        [definition injectProperty:@selector(title) with:NSLocalizedString(@"settings.title", nil)];
    }];
}

@end
