//
//  TSSettingsViewModel.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSViewModel.h"

@class RACSignal;
@protocol RACSubscriber;

NS_ASSUME_NONNULL_BEGIN

@interface TSSettingsViewModel : TSViewModel

@property (assign, nonatomic, readonly) BOOL loading;

@property (assign, nonatomic, readonly) BOOL displayUserImagesEnabled;

- (instancetype)initWithDisplayImagesStateChangesSubscriber:(id<RACSubscriber> __nullable)displayImagesStateChangesSubscriber NS_DESIGNATED_INITIALIZER;

- (RACSignal *)changeDisplayImagesState;

@end

NS_ASSUME_NONNULL_END
