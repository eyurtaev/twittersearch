//
//  SDWebImageManager+RACExtension.h
//
//
//  Created by Evgeniy Yurtaev on 31/12/14.
//  Copyright (c) 2014 Evgeniy Yurtaev. All rights reserved.
//

#import <SDWebImage/SDWebImageManager.h>

@class RACSignal;

@interface SDWebImageManager (RACExtension)

- (RACSignal *)rac_downloadImageWithURL:(NSURL *)url options:(SDWebImageOptions)options;

- (RACSignal *)rac_downloadImageExtendedWithURL:(NSURL *)url options:(SDWebImageOptions)options;

@end
