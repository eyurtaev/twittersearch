//
//  TSServicesAssembly.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSServicesAssembly.h"
#import "TSDataProviderImp.h"
#import "TSCacheImp.h"
#import "TSKeyValueStorageImp.h"

@implementation TSServicesAssembly

- (id)configurer
{
    return [TyphoonDefinition withConfigName:@"Configurator.plist"];
}

- (id<TSDataProvider>)dataProvider
{
    return [TyphoonDefinition withClass:[TSDataProvider class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
        
        [definition useInitializer:@selector(initWithBaseURL:accessToken:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:TyphoonConfig(@"dataProvider.baseURL")];
            [initializer injectParameterWith:TyphoonConfig(@"dataProvider.accessToken")];
        }];
    }];
}

- (id<TSCache>)cache
{
    return [TyphoonDefinition withClass:[TSCache class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
        
        [definition useInitializer:@selector(initWithDatabaseName:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:@"cache_database.sqlite"];
        }];
    }];
}

- (id<TSKeyValueStorage>)keyValueStorage
{
    return [TyphoonDefinition withClass:[TSKeyValueStorage class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
        
        [definition useInitializer:@selector(initWithUserDefaults:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[NSUserDefaults standardUserDefaults]];
        }];
    }];
}

@end
