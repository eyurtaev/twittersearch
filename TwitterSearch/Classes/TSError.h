//
//  TSError.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const TSApiErrorDomain;
extern NSString *const TSAppErrorDomain;
