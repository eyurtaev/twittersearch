//
//  TSTweetsHeaderView.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSTweetsHeaderView.h"

#import "UIFont+TSExtension.h"

@interface TSTweetsHeaderView ()

@property (strong, nonatomic) UILabel *textLabel;

@end

@implementation TSTweetsHeaderView

- (void)commonInit
{
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.font = [UIFont ts_regularFontWithSize:12];
    self.textLabel.minimumScaleFactor = 0.5;
    [self addSubview:self.textLabel];
    
    self.backgroundColor = [UIColor colorWithWhite:1 alpha:0.7];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self commonInit];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.textLabel.frame = UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsetsMake(0, 15, 0, 15));
}

@end
