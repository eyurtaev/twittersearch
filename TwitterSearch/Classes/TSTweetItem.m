//
//  TSTweetItem.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSTweetItem.h"
#import "TSTweet.h"

@interface TSTweetItem ()

@property (copy, nonatomic) NSString *userName;

@property (strong, nonatomic) NSURL *userImageURL;

@property (copy, nonatomic) NSString *text;

@end

@implementation TSTweetItem

- (instancetype)init
{
    return [self initWithUserName:nil userImageURL:nil text:nil];
}

- (instancetype)initWithUserName:(NSString *)userName userImageURL:(NSURL *)userImageURL text:(NSString *)text
{
    self = [super init];
    if (self) {
        self.userName = userName;
        self.userImageURL = userImageURL;
        self.text = text;
    }
    return self;
}

@end
