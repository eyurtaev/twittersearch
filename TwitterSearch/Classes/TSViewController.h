//
//  TSViewController.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 18/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TSViewModel;
@class RACSignal;

NS_ASSUME_NONNULL_BEGIN

@interface TSViewController : UIViewController

@property (strong, nonatomic, nonnull, readonly) TSViewModel *viewModel;

- (instancetype)initWithViewModel:(TSViewModel *)viewModel;

- (void)configureUI NS_REQUIRES_SUPER;
- (void)configureBinding NS_REQUIRES_SUPER;

- (RACSignal *)throbberViewDisplayed;

@end

NS_ASSUME_NONNULL_END
