//
//  TSTweetCell.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TSTweetItem;

NS_ASSUME_NONNULL_BEGIN

@interface TSTweetCell : UITableViewCell

+ (CGFloat)heightWithTweetItem:(TSTweetItem *)item tableView:(UITableView *)tableView;

@property (strong, nonatomic, nullable) TSTweetItem *item;

@end

NS_ASSUME_NONNULL_END
