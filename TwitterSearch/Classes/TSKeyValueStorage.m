//
//  TSKeyValueStorage.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSKeyValueStorage.h"

NSString *const TSKeyValueStorageKeyDisplayUserImagesEnabled = @"TSKeyValueStorageKeyDisplayUserImagesEnabled";
