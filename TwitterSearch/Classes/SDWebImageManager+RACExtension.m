//
//  SDWebImageManager+RACExtension.m
//
//
//  Created by Evgeniy Yurtaev on 31/12/14.
//  Copyright (c) 2014 Evgeniy Yurtaev. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "SDWebImageManager+RACExtension.h"

@implementation SDWebImageManager (RACExtension)

- (RACSignal *)rac_downloadImageWithURL:(NSURL *)url options:(SDWebImageOptions)options
{
    return [[self rac_downloadImageExtendedWithURL:url options:options]
        reduceEach:^id(UIImage *image, id _){
            return image;
        }];
}

- (RACSignal *)rac_downloadImageExtendedWithURL:(NSURL *)url options:(SDWebImageOptions)options
{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        id<SDWebImageOperation> operation = [self downloadImageWithURL:url options:options progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {

            if (image) {
                [subscriber sendNext:RACTuplePack(image, @(cacheType))];
            }
            
            if (error) {
                [subscriber sendError:error];
            } else if (finished) {
                [subscriber sendCompleted];
            }
        }];
        
        return [RACDisposable disposableWithBlock:^{
            [operation cancel];
        }];
    }];
}

@end
