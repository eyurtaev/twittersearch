//
//  TSSwitchTableViewCell.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "TSSwitchTableViewCell.h"

@interface TSSwitchTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISwitch *switchView;

@end

@implementation TSSwitchTableViewCell

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.title = nil;
    self.on = NO;
    self.command = nil;
}

#pragma mark - Properties

- (void)setTitle:(NSString *)title
{
    _title = title;
    
    self.titleLabel.text = _title;
}

- (void)setOn:(BOOL)on
{
    if (_on == on) {
        return;
    }
    _on = on;
    
    [self.switchView setOn:_on animated:NO];
}

- (void)setCommand:(RACCommand *)command
{
    _command = command;
    
    if (_command) {
        RAC(self, switchView.enabled) = [_command.enabled
            takeUntil:self.rac_prepareForReuseSignal];
    }
}

- (IBAction)switchValueChanged:(UISwitch *)sender
{
    [self.command execute:@(sender.on)];
}

@end
