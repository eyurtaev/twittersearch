//
//  TSViewModel.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "TSViewModel.h"

@implementation TSViewModel

- (RACSignal *)prepareForUse
{
    return [RACSignal empty];
}

@end
