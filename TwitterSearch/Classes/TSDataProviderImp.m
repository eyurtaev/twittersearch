//
//  TSDataProviderImp.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <Mantle/Mantle.h>

#import "TSDataProviderImp.h"
#import "TSError.h"
#import "TSTweetsSearchResponse.h"

@interface TSDataProvider ()

@property (strong, nonatomic) AFHTTPSessionManager *sessionManager;

@end

@implementation TSDataProvider

- (instancetype)initWithBaseURL:(NSURL *)baseURL accessToken:(NSString *)accessToken
{
    NSParameterAssert(baseURL);
    NSParameterAssert(accessToken);
    
    self = [super init];
    if (self) {
        self.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        NSString *authorizationHeaderValue = [NSString stringWithFormat:@"Bearer %@", accessToken];
        [self.sessionManager.requestSerializer setValue:authorizationHeaderValue forHTTPHeaderField:@"Authorization"];
    }
    return self;
}

- (RACSignal *)searchTweetsByText:(NSString *)text
{
    return [[RACSignal
        defer:^RACSignal *{
            NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithCapacity:1];
            [parameters setValue:text forKey:@"q"];
            return [self performRequestWithMethod:@"GET" path:@"search/tweets.json" paramters:parameters];
        }]
        flattenMap:^RACStream *(RACTuple *response) {
            NSDictionary *responseObject = [response second];
            return [self modelOfClass:[TSTweetsSearchResponse class] fromJSONDictionary:responseObject];
        }];
}

#pragma mark - Private methods

- (RACSignal *)performRequestWithMethod:(NSString *)method path:(NSString *)path paramters:(NSDictionary *)parameters
{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSURL *requestURL = [NSURL URLWithString:path relativeToURL:self.sessionManager.baseURL];
        
        NSError *error;
        NSMutableURLRequest *request = [self.sessionManager.requestSerializer
            requestWithMethod:method
            URLString:[requestURL absoluteString]
            parameters:parameters
            error:&error];
        
        if (error) {
            [subscriber sendError:error];
            return nil;
        }
        
        NSURLSessionTask *task = [self.sessionManager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            if (!error) {
                NSError *responseObjectError = [self checkResponseObjectToError:responseObject];
                if (!responseObjectError) {
                    [subscriber sendNext:RACTuplePack(response, responseObject)];
                    [subscriber sendCompleted];
                } else {
                    [subscriber sendError:responseObjectError];
                }
            } else {
                [subscriber sendError:error];
            }
        }];
        [task resume];
        
        return [RACDisposable disposableWithBlock:^{
            [task cancel];
        }];
    }];
}

- (NSError *)checkResponseObjectToError:(NSDictionary *)responseObject
{
    if (![responseObject isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    
    NSError *error;
    NSArray *errors = responseObject[@"errors"];
    if (errors && [errors isKindOfClass:[NSArray class]]) {
        NSDictionary *errorObject = errors.firstObject;
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithCapacity:1];
        [userInfo setValue:errorObject[@"message"] forKey:NSLocalizedDescriptionKey];
        NSInteger code = [errorObject[@"code"] integerValue];
        
        error = [NSError errorWithDomain:TSApiErrorDomain code:code userInfo:userInfo];
    }
    return error;
}

- (RACSignal *)modelOfClass:(Class)modelClass fromJSONDictionary:(NSDictionary *)JSONDictionary
{
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        if (![JSONDictionary isKindOfClass:[NSDictionary class]]) {
            NSError *error = [NSError errorWithDomain:TSAppErrorDomain code:0 userInfo:@{
                NSLocalizedDescriptionKey: NSLocalizedString(@"data_provider.error.incorrect_response_object_type.description", nil),
            }];
            [subscriber sendError:error];
            return nil;
        }
        NSError *error;
        id object = [MTLJSONAdapter modelOfClass:modelClass fromJSONDictionary:JSONDictionary error:&error];
        if (!error) {
            [subscriber sendNext:object];
            [subscriber sendCompleted];
        } else {
            [subscriber sendError:error];
        }
        return nil;
    }];
}

@end
