//
//  NSValueTransformer+TSExtension.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 18/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <Mantle/Mantle.h>

#import "NSValueTransformer+TSExtension.h"

NSString *const TSTimestampValueTransformer = @"TSTimestampValueTransformer";
NSString *const TSTransformerErrorHandlingErrorDomain = @"TSTransformerErrorHandlingErrorDomain";

@implementation NSValueTransformer (TSExtension)

+ (void)load
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"eee MMM dd HH:mm:ss Z yyyy";
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    
    MTLValueTransformer *timestampValueTransformer = [MTLValueTransformer transformerUsingForwardBlock:^id(NSString *value, BOOL *success, NSError *__autoreleasing *error) {
        if (![value isKindOfClass:[NSString class]]) {
            *success = NO;
            *error = [NSError errorWithDomain:TSTransformerErrorHandlingErrorDomain
                code:0
                userInfo:@{
                    NSLocalizedDescriptionKey: NSLocalizedString(@"date_transformer.error.could_not_convert_string_to_date", nil),
                }];
            return nil;
        }
        NSDate *date = [dateFormatter dateFromString:value];
        if (!date) {
            *success = NO;
            *error = [NSError errorWithDomain:TSTransformerErrorHandlingErrorDomain
                code:0
                userInfo:@{
                    NSLocalizedDescriptionKey: NSLocalizedString(@"date_transformer.error.could_not_convert_string_to_date", nil),
                }];
            return nil;
        }
        NSTimeInterval timestamp = [date timeIntervalSince1970];
        
        return @(timestamp);
        
    } reverseBlock:^id(NSNumber *timestamp, BOOL *success, NSError *__autoreleasing *error) {
        if (![timestamp isKindOfClass:[NSNumber class]]) {
            return nil;
        }
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timestamp doubleValue]];
        NSString *value = [dateFormatter stringFromDate:date];
        
        return value;
    }];
    [NSValueTransformer setValueTransformer:timestampValueTransformer forName:TSTimestampValueTransformer];
}

@end
