//
//  UIFont+TSExtension.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "UIFont+TSExtension.h"

@implementation UIFont (TSExtension)

+ (instancetype)ts_regularFontWithSize:(CGFloat)size
{
    return [self systemFontOfSize:size];
}

+ (instancetype)ts_boldFontWithSize:(CGFloat)size
{
    return [self boldSystemFontOfSize:size];
}

@end
