//
//  TSSwitchTableViewCell.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RACCommand;

@interface TSSwitchTableViewCell : UITableViewCell

@property (copy, nonatomic) NSString *title;

@property (assign, nonatomic) BOOL on;

@property (strong, nonatomic) RACCommand *command;

@end
