//
//  TSTweetsSearchResponse.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <libextobjc/EXTKeyPathCoding.h>

#import "TSTweetsSearchResponse.h"
#import "TSTweet.h"

@implementation TSTweetsSearchResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
        @keypath([TSTweetsSearchResponse new], tweets): @"statuses",
    };
}

+ (NSValueTransformer *)tweetsJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[TSTweet class]];
}

@end
