//
//  TSTweet.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 18/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <Mantle/Mantle.h>
#import <MTLFMDBAdapter/MTLFMDBAdapter.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSTweet : MTLModel<MTLJSONSerializing, MTLFMDBSerializing>

@property (copy, nonatomic, nullable) NSString *tweeetId;

@property (copy, nonatomic, nullable) NSString *text;

@property (strong, nonatomic, nullable) NSNumber *createdAtTimestamp;

@property (copy, nonatomic, nullable) NSString *userName;

@property (strong, nonatomic, nullable) NSString *userImageURLString;

- (NSDate *)createdAt;

- (NSURL *)userImageURL;

@end

NS_ASSUME_NONNULL_END
