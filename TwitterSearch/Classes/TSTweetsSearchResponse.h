//
//  TSTweetsSearchResponse.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <Mantle/Mantle.h>

@class TSTweet;

@interface TSTweetsSearchResponse : MTLModel<MTLJSONSerializing>

@property (copy, nonatomic, nullable) NSArray<TSTweet *> *tweets;

@end
