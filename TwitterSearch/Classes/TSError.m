//
//  TSError.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSError.h"

NSString *const TSApiErrorDomain = @"com.twittersearch.error.api";
NSString *const TSAppErrorDomain = @"com.twittersearch.error.application";
