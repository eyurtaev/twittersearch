//
//  TSTweetsHeaderView.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSTweetsHeaderView : UIView

@property (strong, nonatomic, nonnull, readonly) UILabel *textLabel;

@end
