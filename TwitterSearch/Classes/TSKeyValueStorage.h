//
//  TSKeyValueStorage.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;

NS_ASSUME_NONNULL_BEGIN

extern NSString *const TSKeyValueStorageKeyDisplayUserImagesEnabled;

@protocol TSKeyValueStorage <NSObject>

- (RACSignal *)objectForKey:(NSString *)key;

- (RACSignal *)setObject:(id __nullable)object forKey:(NSString *)key;

- (RACSignal *)removeObjectForKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
