//
//  TSSettingsViewController.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <libextobjc/EXTScope.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

#import "TSSettingsViewController.h"
#import "TSSettingsViewModel.h"
#import "TSSwitchTableViewCell.h"

@interface TSSettingsViewController () <UITableViewDataSource>

@property (strong, nonatomic) TSSettingsViewModel *viewModel;

@property (strong, nonatomic) RACCommand *changeImageDisplayStateCommand;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation TSSettingsViewController

@dynamic viewModel;

- (void)configureUI
{
    [super configureUI];
    
    UINib *switchCellNib = [UINib nibWithNibName:NSStringFromClass([TSSwitchTableViewCell class]) bundle:nil];
    [self.tableView registerNib:switchCellNib forCellReuseIdentifier:NSStringFromClass([TSSwitchTableViewCell class])];
}

- (void)configureBinding
{
    [super configureBinding];
    
    @weakify(self);
    self.changeImageDisplayStateCommand = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        return [self.viewModel changeDisplayImagesState];
    }];
    [[self.viewModel prepareForUse] subscribeCompleted:^{}];
}

- (RACSignal *)throbberViewDisplayed
{
    return RACObserve(self, viewModel.loading);
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TSSwitchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TSSwitchTableViewCell class]) forIndexPath:indexPath];
    cell.title = NSLocalizedString(@"settings.display_images.title", nil);
    RAC(cell, on) = [RACObserve(self, viewModel.displayUserImagesEnabled)
        takeUntil:cell.rac_prepareForReuseSignal];
    cell.command = self.changeImageDisplayStateCommand;
    
    return cell;
}

@end
