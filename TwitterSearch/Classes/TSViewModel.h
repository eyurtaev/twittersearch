//
//  TSViewModel.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSRouter.h"
#import "TSDataProvider.h"
#import "TSCache.h"
#import "TSKeyValueStorage.h"

@class RACSignal;

NS_ASSUME_NONNULL_BEGIN

@interface TSViewModel : NSObject

@property (strong, nonatomic, nullable) id<TSRouter> router;

@property (strong, nonatomic, nullable) id<TSDataProvider> dataProvider;

@property (strong, nonatomic, nullable) id<TSCache> cache;

@property (strong, nonatomic, nullable) id<TSKeyValueStorage> keyValueStorage;

- (RACSignal *)prepareForUse;

@end

NS_ASSUME_NONNULL_END
