//
//  TSKeyValueStorageImp.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "TSKeyValueStorageImp.h"

@interface TSKeyValueStorage ()

@property (strong, nonatomic) NSUserDefaults *userDefaults;

@end

@implementation TSKeyValueStorage

- (instancetype)init
{
    NSAssert(NO, @"Use `initWithUserDefaults:`");
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wnonnull"
    return [self initWithUserDefaults:nil];
#pragma clang diagnostic pop
}

- (instancetype)initWithUserDefaults:(NSUserDefaults *)userDefaults
{
    NSParameterAssert(userDefaults);
    
    self = [super init];
    if (self) {
        self.userDefaults = userDefaults;
    }
    return self;
}

- (RACSignal *)objectForKey:(NSString *)key
{
    NSParameterAssert(key);
    
    return [RACSignal defer:^RACSignal *{
        id object = [self.userDefaults objectForKey:key];
        return [RACSignal return:object];
    }];
}

- (RACSignal *)setObject:(id)object forKey:(NSString *)key
{
    NSParameterAssert(key);
    
    return [RACSignal defer:^RACSignal *{
        [self.userDefaults setValue:object forKey:key];
        [self.userDefaults synchronize];
        
        return [RACSignal empty];
    }];
}

- (RACSignal *)removeObjectForKey:(NSString *)key
{
    return [RACSignal defer:^RACSignal *{
        [self.userDefaults removeObjectForKey:key];
        [self.userDefaults synchronize];
        
        return [RACSignal empty];
    }];
}

@end
