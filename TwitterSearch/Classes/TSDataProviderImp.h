//
//  TSDataProviderImp.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSDataProvider.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSDataProvider : NSObject<TSDataProvider>

- (instancetype)initWithBaseURL:(NSURL *)baseURL accessToken:(NSString *)accessToken;

@end

NS_ASSUME_NONNULL_END
