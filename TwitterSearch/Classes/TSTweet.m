//
//  TSTweet.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 18/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <libextobjc/EXTKeyPathCoding.h>

#import "TSTweet.h"
#import "NSValueTransformer+TSExtension.h"

@implementation TSTweet

#pragma mark - MTLJSONSerializing

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
        @keypath([TSTweet new], tweeetId): @"id_str",
        @keypath([TSTweet new], text): @"text",
        @keypath([TSTweet new], createdAtTimestamp): @"created_at",
        @keypath([TSTweet new], userName): @"user.name",
        @keypath([TSTweet new], userImageURLString): @"user.profile_image_url",
    };
}

+ (NSValueTransformer *)createdAtTimestampJSONTransformer
{
    return [NSValueTransformer valueTransformerForName:TSTimestampValueTransformer];
}

- (NSDate *)createdAt
{
    NSDate *createdAt;
    if (self.createdAtTimestamp) {
        createdAt = [NSDate dateWithTimeIntervalSince1970:[self.createdAtTimestamp doubleValue]];
    }
    return createdAt;
}

- (NSURL *)userImageURL
{
    NSURL *userImageURL;
    if (self.userImageURLString.length > 0) {
        userImageURL = [NSURL URLWithString:self.userImageURLString];
    }
    return userImageURL;
}

#pragma mark - MTLFMDBSerializing

+ (NSDictionary *)FMDBColumnsByPropertyKey
{
    return @{
        @keypath([TSTweet new], tweeetId): @"tweet_id",
        @keypath([TSTweet new], createdAtTimestamp): @"created_at",
        @keypath([TSTweet new], userName): @"user_name",
        @keypath([TSTweet new], userImageURLString): @"user_image_url",
    };
}

+ (NSArray *)FMDBPrimaryKeys
{
    return @[
        @"tweet_id",
    ];
}

+ (NSString *)FMDBTableName
{
    return @"tweet";
}

@end
