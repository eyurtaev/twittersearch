//
//  TSKeyValueStorageImp.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSKeyValueStorage.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSKeyValueStorage : NSObject<TSKeyValueStorage>

- (instancetype)initWithUserDefaults:(NSUserDefaults *)userDefaults NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
