//
//  TSTweetsSearchViewModel.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSViewModel.h"

@class TSTweetItem;
@protocol TSDataProvider;
@protocol TSKeyValueStorage;
@protocol TSCache;
@protocol TSRouter;

NS_ASSUME_NONNULL_BEGIN

@interface TSTweetsSearchViewModel : TSViewModel

@property (assign, nonatomic, readonly) BOOL loading;

@property (copy, nonatomic, nullable, readonly) NSString *message;

@property (copy, nonatomic, nullable, readonly) NSArray<TSTweetItem *> *items;

- (instancetype)initWithSearchText:(NSString *)searchText refreshIntervel:(NSTimeInterval)refreshIntervel;

- (RACSignal *)showSettings;

@end

NS_ASSUME_NONNULL_END
