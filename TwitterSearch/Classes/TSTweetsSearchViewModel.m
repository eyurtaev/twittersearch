//
//  TSTweetsSearchViewModel.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>
#import <libextobjc/EXTScope.h>
#import <libextobjc/EXTSelectorChecking.h>
#import <TTTLocalizedPluralString/TTTLocalizedPluralString.h>
#import <math.h>

#import "TSTweetsSearchViewModel.h"
#import "TSTweet.h"
#import "TSTweetsSearchResponse.h"
#import "TSTweetItem.h"

@interface TSTweetsSearchViewModel ()

@property (assign, nonatomic) BOOL loading;

@property (copy, nonatomic) NSString *message;

@property (copy, nonatomic) NSArray<TSTweetItem *> *items;

@property (copy, nonatomic) NSString *searchText;

@property (assign, nonatomic) NSTimeInterval refreshInterval;

@property (assign, nonatomic) BOOL userImagesDisplayEnabled;

@property (copy, nonatomic) NSArray<TSTweet *> *tweets;

@property (assign, nonatomic) BOOL tweetsRefresingEnabled;

@property (strong, nonatomic) NSDate *nextRefreshDate;

@property (strong, nonatomic) NSTimer *refreshTimer;

@end

@implementation TSTweetsSearchViewModel

- (void)dealloc
{
    [self.refreshTimer invalidate];
}

- (instancetype)initWithSearchText:(NSString *)searchText refreshIntervel:(NSTimeInterval)refreshIntervel
{
    self = [super init];
    if (self) {
        self.searchText = searchText;
        self.refreshInterval = refreshIntervel;
        
        @weakify(self);
        RAC(self, items) = [[RACSignal combineLatest:@[ RACObserve(self, userImagesDisplayEnabled), RACObserve(self, tweets) ]]
            reduceEach:^id(NSNumber *imagesEnabledNumber, NSArray *tweets){
                NSArray *sortedTweets = [tweets sortedArrayUsingComparator:^NSComparisonResult(TSTweet *obj1, TSTweet *obj2) {
                    return [obj2.createdAtTimestamp compare:obj1.createdAtTimestamp];
                }];
                
                BOOL imagesEnabled = [imagesEnabledNumber boolValue];
                return [sortedTweets.rac_sequence map:^id(TSTweet *tweet) {
                    @strongify(self);
                    return [self tweetItemWithTweet:tweet imageEnabled:imagesEnabled];
                }].array;
            }];
    }
    return self;
}

- (RACSignal *)prepareForUse
{
    return [[[[[[[self.keyValueStorage objectForKey:TSKeyValueStorageKeyDisplayUserImagesEnabled]
        doNext:^(NSNumber *enabled) {
            self.userImagesDisplayEnabled = [enabled boolValue];
        }]
        then:^RACSignal *{
            return [[self.cache allObjectsForClass:[TSTweet class]] catchTo:[RACSignal empty]];
        }]
        doNext:^(NSArray *tweets) {
            self.tweets = tweets;
        }]
        then:^RACSignal *{
            return [self refreshTweets];
        }]
        finally:^{
            if (self.refreshInterval > 0) {
                self.tweetsRefresingEnabled = YES;
            }
        }]
        ignoreValues];
}

- (RACSignal *)showSettings
{
    @weakify(self);
    return [RACSignal defer:^RACSignal *{
        RACSubject *subject = [RACReplaySubject replaySubjectWithCapacity:1];
        [subject subscribeNext:^(NSNumber *imagesEnabled) {
            @strongify(self);
            self.userImagesDisplayEnabled = [imagesEnabled boolValue];
        }];
        return [self.router showSettingsWithDisplayImagesStateChangesSubscriber:subject];
    }];
}

- (void)refreshTimerTick:(NSTimer *)timer
{
    NSTimeInterval timeInterval = [self.nextRefreshDate timeIntervalSinceDate:[NSDate date]];
    NSUInteger secondsBeforeUpdate = MAX(round(timeInterval), 0);
    
    if (secondsBeforeUpdate > 0) {
        self.message = [self messageWithSecondsBeforeRefresh:secondsBeforeUpdate];
        return;
    }
    
    self.tweetsRefresingEnabled = NO;
    @weakify(self);
    [[self refreshTweets] subscribeCompleted:^{
        @strongify(self);
        self.tweetsRefresingEnabled = YES;
    }];
}

#pragma mark - Properties

- (void)setTweetsRefresingEnabled:(BOOL)tweetsRefresingEnabled
{
    if (_tweetsRefresingEnabled == tweetsRefresingEnabled) {
        return;
    }
    _tweetsRefresingEnabled = tweetsRefresingEnabled;
    
    if (_tweetsRefresingEnabled) {
        self.refreshTimer = [NSTimer timerWithTimeInterval:1/2.0
            target:self
            selector:@checkselector(self, refreshTimerTick:)
            userInfo:nil
            repeats:YES];
        self.nextRefreshDate = [NSDate dateWithTimeIntervalSinceNow:self.refreshInterval];
        [[NSRunLoop currentRunLoop] addTimer:self.refreshTimer forMode:NSRunLoopCommonModes];
        
    } else {
        [self.refreshTimer invalidate];
        self.refreshTimer = nil;
        self.nextRefreshDate = nil;
    }
}

#pragma mark - Private methods

- (RACSignal *)refreshTweets
{
    @weakify(self);
    return [[[[[self searchAndSaveTweetsWithText:self.searchText]
        initially:^{
            @strongify(self);
            self.message = NSLocalizedString(@"tweets_search.message.loading", nil);
            if (self.tweets.count < 1) {
                self.loading = YES;
            }
        }]
        finally:^{
            @strongify(self);
            self.message = [self messageWithSecondsBeforeRefresh:self.refreshInterval];
            self.loading = NO;
        }]
        doNext:^(NSArray *tweets) {
            @strongify(self);
            self.tweets = tweets;
        }]
        catch:^RACSignal *(NSError *error) {
            @strongify(self);
            return [self.router showMessageWithError:error];
        }];
}

- (RACSignal *)searchAndSaveTweetsWithText:(NSString *)text
{
    @weakify(self);
    return [[self.dataProvider searchTweetsByText:text]
        flattenMap:^RACStream *(TSTweetsSearchResponse *response) {
            @strongify(self);
            
            return [[self.cache saveObjects:response.tweets] then:^RACSignal *{
                return [RACSignal return:response.tweets];
            }];
        }];
}

- (TSTweetItem *)tweetItemWithTweet:(TSTweet *)tweet imageEnabled:(BOOL)imageEnabled
{
    NSURL *userImageURL = imageEnabled ? [tweet userImageURL] : nil;
    TSTweetItem *item = [[TSTweetItem alloc] initWithUserName:tweet.userName userImageURL:userImageURL text:tweet.text];
    
    return item;
}

- (NSString *)messageWithSecondsBeforeRefresh:(NSUInteger)secondsBeforeRefresh
{
    NSString *messageSuffix = TTTLocalizedPluralString(secondsBeforeRefresh, @"tweets_search.message.seconds", nil);
    NSString *message = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"tweets_search.message.prefix_seconds", nil), messageSuffix];

    return message;
}

@end
