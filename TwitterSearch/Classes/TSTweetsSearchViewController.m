//
//  TSTweetsSearchViewController.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <libextobjc/EXTScope.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

#import "TSTweetsSearchViewController.h"
#import "TSTweetsSearchViewModel.h"
#import "TSImageTweetCell.h"
#import "TSTweetCell.h"
#import "TSTweetItem.h"
#import "TSTweetsHeaderView.h"

#import "UIFont+TSExtension.h"

@interface TSTweetsSearchViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) TSTweetsSearchViewModel *viewModel;

@property (copy, nonatomic) NSArray<TSTweetItem *> *tweetsItems;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) TSTweetsHeaderView *messageHeaderView;

@property (strong, nonatomic) UIButton *settingsButton;

@end

@implementation TSTweetsSearchViewController

@dynamic viewModel;

- (void)configureUI
{
    [super configureUI];
    
    self.messageHeaderView = [[TSTweetsHeaderView alloc] init];
    
    UINib *imageTweetCellNib = [UINib nibWithNibName:NSStringFromClass([TSImageTweetCell class]) bundle:nil];
    [self.tableView registerNib:imageTweetCellNib forCellReuseIdentifier:NSStringFromClass([TSImageTweetCell class])];
    
    UINib *tweetCellNib = [UINib nibWithNibName:NSStringFromClass([TSTweetCell class]) bundle:nil];
    [self.tableView registerNib:tweetCellNib forCellReuseIdentifier:NSStringFromClass([TSTweetCell class])];
    
    self.settingsButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.settingsButton setTitle:NSLocalizedString(@"tweets_search.settings_button.title", nil) forState:UIControlStateNormal];
    [self.settingsButton sizeToFit];
    
    UIBarButtonItem *settingsItem = [[UIBarButtonItem alloc] initWithCustomView:self.settingsButton];
    self.navigationItem.rightBarButtonItem = settingsItem;
}

- (void)configureBinding
{
    [super configureBinding];
    
    RAC(self, messageHeaderView.textLabel.text) = RACObserve(self, viewModel.message);
    RAC(self, tweetsItems) = RACObserve(self, viewModel.items);
    
    @weakify(self);
    self.settingsButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        return [self.viewModel showSettings];
    }];
    
    [[self.viewModel prepareForUse] subscribeCompleted:^{}];
}

- (RACSignal *)throbberViewDisplayed
{
    return RACObserve(self, viewModel.loading);
}

#pragma mark - Properties

- (void)setTweetsItems:(NSArray<TSTweetItem *> *)tweetsItems
{
    _tweetsItems = tweetsItems;
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = 0;
    TSTweetItem *item = self.tweetsItems[indexPath.row];
    if (item.userImageURL) {
        cellHeight = [TSImageTweetCell heightWithTweetItem:item tableView:tableView];
    } else {
        cellHeight = [TSTweetCell heightWithTweetItem:item tableView:tableView];
    }
    return cellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return self.messageHeaderView;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = self.tweetsItems.count;
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    TSTweetItem *item = self.tweetsItems[indexPath.row];
    if (item.userImageURL) {
        TSImageTweetCell *tweetCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TSImageTweetCell class]) forIndexPath:indexPath];
        tweetCell.item = item;
        cell = tweetCell;
    } else {
        TSTweetCell *tweetCell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TSTweetCell class]) forIndexPath:indexPath];
        tweetCell.item = item;
        cell = tweetCell;
    }
    return cell;
}

@end
