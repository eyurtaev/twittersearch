//
//  NSValueTransformer+TSExtension.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 18/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const TSTimestampValueTransformer;
extern NSString *const TSTransformerErrorHandlingErrorDomain;

@interface NSValueTransformer (TSExtension)

@end
