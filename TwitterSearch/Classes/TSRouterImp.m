//
//  TSRouterImp.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <ReactiveCocoa/ReactiveCocoa.h>

#import "TSRouterImp.h"
#import "TSNavigationController.h"
#import "TSViewController.h"
#import "TSViewControllersAssembly.h"
#import "TSTweetsSearchViewController.h"
#import "TSSettingsViewController.h"

@interface TSRouter ()

@property (strong, nonatomic) TSNavigationController *navigationController;

@end

@implementation TSRouter

- (instancetype)init
{
    NSAssert(NO, @"Use `initWithWindow:`");
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wnonnull"
    return [self initWithWindow:nil];
#pragma clang diagnostic pop
}

- (instancetype)initWithWindow:(UIWindow *)window
{
    self = [super init];
    if (self) {
        self.navigationController = [[TSNavigationController alloc] init];
        window.rootViewController = self.navigationController;
    }
    return self;
}

- (RACSignal *)returnToViewModel:(TSViewModel *)viewModel;
{
    return [RACSignal defer:^RACSignal *{
        UIViewController *targetViewController = [self.navigationController.viewControllers.rac_sequence
            objectPassingTest:^BOOL(TSViewController *viewController) {
                if (![viewController isKindOfClass:[TSViewController class]]) {
                    return NO;
                }
                BOOL isTargetViewModel = (viewController.viewModel == viewModel);
                
                return isTargetViewModel;
            }];
        NSAssert(targetViewController, @"view controller with %@ view model should contained in navigation controller stack", viewModel);
        
        return [self.navigationController rac_popToViewController:targetViewController animated:YES];
    }];
}

- (RACSignal *)showMessageWithError:(NSError *)error
{
    return [RACSignal defer:^RACSignal *{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error localizedDescription]
            message:[error localizedFailureReason]
            delegate:nil
            cancelButtonTitle:NSLocalizedString(@"alert.button.ok", nil)
            otherButtonTitles:nil];
        [alertView show];
        
        return [alertView.rac_buttonClickedSignal ignoreValues];
    }];
}

- (RACSignal *)showInitialScreen
{
    return [RACSignal defer:^RACSignal *{
        UIViewController *viewController = [self.viewControllersAssembly tweetsSearchViewController];
        return [self.navigationController rac_setViewControllers:@[ viewController ] animated:NO];
    }];
}

- (RACSignal *)showSettingsWithDisplayImagesStateChangesSubscriber:(id<RACSubscriber>)displayImagesStateChangesSubscriber
{
    return [RACSignal defer:^RACSignal *{
        UIViewController *viewController = [self.viewControllersAssembly settingsViewControllerWithDisplayImagesStateChangesSubscriber:displayImagesStateChangesSubscriber];
        return [self.navigationController rac_pushViewController:viewController animated:YES];
    }];
}

#pragma mark - Private methods

- (RACSignal *)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    return [self.navigationController rac_pushViewController:viewController animated:animated];
}

@end
