//
//  TSRouter.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RACSubscriber;
@class TSViewModel;
@class RACSignal;

NS_ASSUME_NONNULL_BEGIN

@protocol TSRouter <NSObject>

- (RACSignal *)returnToViewModel:(TSViewModel *)viewModel;

- (RACSignal *)showMessageWithError:(NSError *)error;

- (RACSignal *)showInitialScreen;

- (RACSignal *)showSettingsWithDisplayImagesStateChangesSubscriber:(id<RACSubscriber> __nullable)displayImagesStateChangesSubscriber;

@end

NS_ASSUME_NONNULL_END
