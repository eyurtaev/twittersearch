//
//  TSViewControllersAssembly.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <Typhoon/Typhoon.h>

@protocol RACSubscriber;
@class TSTweetsSearchViewController;
@class TSSettingsViewController;

@interface TSViewControllersAssembly : TyphoonAssembly

- (TSTweetsSearchViewController *)tweetsSearchViewController;

- (TSSettingsViewController *)settingsViewControllerWithDisplayImagesStateChangesSubscriber:(id<RACSubscriber>)displayImagesStateChangesSubscriber;

@end
