//
//  TSCache.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 18/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RACSignal;
@class MTLModel;
@protocol MTLFMDBSerializing;

NS_ASSUME_NONNULL_BEGIN

@protocol TSCache <NSObject>

- (RACSignal *)saveObject:(MTLModel<MTLFMDBSerializing> *)object;
- (RACSignal *)saveObjects:(NSArray<MTLModel<MTLFMDBSerializing> *> *)objects;

- (RACSignal *)allObjectsForClass:(Class)objectsClass;

@end

NS_ASSUME_NONNULL_END
