//
//  UIFont+TSExtension.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIFont (TSExtension)

+ (instancetype)ts_regularFontWithSize:(CGFloat)size;

+ (instancetype)ts_boldFontWithSize:(CGFloat)size;

@end

NS_ASSUME_NONNULL_END
