//
//  TSViewModelsAssembly.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSViewModelsAssembly.h"
#import "TSServicesAssembly.h"
#import "TSViewModel.h"
#import "TSTweetsSearchViewModel.h"
#import "TSSettingsViewModel.h"

@interface TSViewModelsAssembly ()

@property (strong, nonatomic) TSServicesAssembly *servicesAssembly;

@end

@implementation TSViewModelsAssembly

- (id)configurer
{
    return [TyphoonDefinition withConfigName:@"Configurator.plist"];
}

- (TSViewModel *)viewModel
{
    return [TyphoonDefinition withClass:[TSViewModel class] configuration:^(TyphoonDefinition *definition) {
        definition.abstract = YES;
        [definition injectProperty:@selector(router)];
        [definition injectProperty:@selector(dataProvider) with:self.servicesAssembly.dataProvider];
        [definition injectProperty:@selector(cache) with:self.servicesAssembly.cache];
        [definition injectProperty:@selector(keyValueStorage) with:self.servicesAssembly.keyValueStorage];
    }];
}

- (TSTweetsSearchViewModel *)tweetsSearchViewModel
{
    return [TyphoonDefinition withClass:[TSTweetsSearchViewModel class] configuration:^(TyphoonDefinition *definition) {
        definition.parent = [self viewModel];
        
        [definition useInitializer:@selector(initWithSearchText:refreshIntervel:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:TyphoonConfig(@"tweetsSearch.text")];
            [initializer injectParameterWith:TyphoonConfig(@"tweetsSearch.refreshInterval")];
        }];
    }];
}

- (TSSettingsViewModel *)settingsViewModelWithDisplayImagesStateChangesSubscriber:(id<RACSubscriber>)displayImagesStateChangesSubscriber
{
    return [TyphoonDefinition withClass:[TSSettingsViewModel class] configuration:^(TyphoonDefinition *definition) {
        definition.parent = [self viewModel];
        
        [definition useInitializer:@selector(initWithDisplayImagesStateChangesSubscriber:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:displayImagesStateChangesSubscriber];
        }];
    }];
}

@end
