//
//  TSAppDelegate.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSRouter.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSAppDelegate : NSObject<UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) id<TSRouter> router;

@end

NS_ASSUME_NONNULL_END
