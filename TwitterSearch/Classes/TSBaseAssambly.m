//
//  TSBaseAssambly.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSBaseAssambly.h"
#import "TSAppDelegate.h"
#import "TSRouterImp.h"
#import "TSViewControllersAssembly.h"

@interface TSBaseAssambly ()

@property (strong, nonatomic) TSViewControllersAssembly *viewControllersAssembly;

@end

@implementation TSBaseAssambly

- (id<UIApplicationDelegate>)appDelegate
{
    return [TyphoonDefinition withClass:[TSAppDelegate class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
        
        [definition injectProperty:@selector(window) with:[self window]];
        [definition injectProperty:@selector(router) with:[self router]];
    }];
}

- (UIWindow *)window
{
    return [TyphoonDefinition withClass:[UIWindow class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
        
        [definition useInitializer:@selector(initWithFrame:) parameters:^(TyphoonMethod *initializer) {
            CGRect screenBounds = [UIScreen mainScreen].bounds;
            [initializer injectParameterWith:[NSValue valueWithCGRect:screenBounds]];
        }];
    }];
}

- (id<TSRouter>)router
{
    return [TyphoonDefinition withClass:[TSRouter class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
        
        [definition useInitializer:@selector(initWithWindow:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[self window]];
        }];
        [definition injectProperty:@selector(viewControllersAssembly) with:self.viewControllersAssembly];
    }];
}

@end
