//
//  TSTweetCell.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 20/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import "TSTweetCell.h"
#import "TSTweetItem.h"

#import "UIFont+TSExtension.h"

#define USER_NAME_LABEL_FONT ([UIFont ts_regularFontWithSize:17])
#define TWEET_TEXT_LABEL_FONT ([UIFont ts_regularFontWithSize:15])

@interface TSTweetCell ()

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *tweetTextLabel;

@end

@implementation TSTweetCell

+ (CGFloat)heightWithTweetItem:(TSTweetItem *)item tableView:(UITableView *)tableView
{
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(8, 15, 8, 15);
    CGFloat spaceBetweenLabels = 8;
    
    CGFloat labelsWidth = CGRectGetWidth(tableView.frame) - contentInsets.left - contentInsets.right;
    CGRect nameLabelBounding = [item.userName boundingRectWithSize:CGSizeMake(labelsWidth, INFINITY)
        options:NSStringDrawingUsesLineFragmentOrigin
        attributes:@{
            NSFontAttributeName: USER_NAME_LABEL_FONT,
        }
        context:nil];
    
    CGRect textLabelBounding = [item.text boundingRectWithSize:CGSizeMake(labelsWidth, INFINITY)
        options:NSStringDrawingUsesLineFragmentOrigin
        attributes:@{
            NSFontAttributeName: TWEET_TEXT_LABEL_FONT,
        }
        context:nil];
    CGFloat cellHeight = CGRectGetHeight(nameLabelBounding) + CGRectGetHeight(textLabelBounding) + contentInsets.top + contentInsets.bottom + spaceBetweenLabels + 1;
    
    return ceil(cellHeight);
}

- (void)commonInit
{
    self.userNameLabel.font = USER_NAME_LABEL_FONT;
    self.tweetTextLabel.font = TWEET_TEXT_LABEL_FONT;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self commonInit];

    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self commonInit];
}

#pragma mark - Properties

- (void)setItem:(TSTweetItem *)item
{
    _item = item;
    
    self.userNameLabel.text = item.userName;
    self.tweetTextLabel.text = item.text;
}

@end
