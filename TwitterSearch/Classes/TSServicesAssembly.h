//
//  TSServicesAssembly.h
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 19/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <Typhoon/Typhoon.h>

#import "TSDataProvider.h"
#import "TSKeyValueStorage.h"
#import "TSCache.h"

@interface TSServicesAssembly : TyphoonAssembly

- (id<TSDataProvider>)dataProvider;

- (id<TSCache>)cache;

- (id<TSKeyValueStorage>)keyValueStorage;

@end
