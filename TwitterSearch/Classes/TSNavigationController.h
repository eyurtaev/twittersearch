//
//  TSNavigationController.h
//
//
//  Created by Evgeniy Yurtaev on 17.06.15.
//
//

#import <UIKit/UIKit.h>

@class RACSignal;

NS_ASSUME_NONNULL_BEGIN

@interface TSNavigationController : UINavigationController

- (RACSignal *)rac_setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated;
- (RACSignal *)rac_pushViewController:(UIViewController *)viewController animated:(BOOL)animated;
- (RACSignal *)rac_popViewControllerAnimated:(BOOL)animated;
- (RACSignal *)rac_popToViewController:(UIViewController *)viewController animated:(BOOL)animated;
- (RACSignal *)rac_popToRootViewControllerAnimated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
