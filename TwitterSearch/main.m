//
//  main.m
//  TwitterSearch
//
//  Created by Evgeniy Yurtaev on 18/12/15.
//  Copyright © 2015 Evgeniy Yurtaev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TSAppDelegate class]));
    }
}
